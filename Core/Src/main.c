/* USER CODE BEGIN Header */
/**
  **************************
  * @file           : main.c
  * @brief          : Main program body
  **************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  **************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "fatfs.h"
#include "sd.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <sinewave.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

DAC_HandleTypeDef hdac;
DMA_HandleTypeDef hdma_dac1;

SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;

UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart2_tx;

/* USER CODE BEGIN PV */



volatile int	BUTTON_DEBOUNCE_DELAY = 50; //ms

volatile int	LED_DELAY_TIME = 250; // ms
volatile int 	REC_DELAY_TIME = 20480;
volatile int 	PLAY_DELAY_TIME = 20480;




float	LED1_FIRST_FLASH = 0.0;
float	LED2_FIRST_FLASH = 0.0;
float	LED3_FIRST_FLASH = 0.0;
float	LED4_FIRST_FLASH = 0.0;
float	REC_START_TICK = 0.0;
float	PLAY_START_TICK = 0.0;



extern volatile uint16_t	PLAY_SLOT1 = 1;
extern volatile uint16_t	PLAY_SLOT2 = 1;
extern volatile uint16_t	PLAY_SLOT3 = 1;
extern uint16_t	RECORDING = 0;




float	S1_BUTTON_STOP_FIRST = 0.0;
float	S2_BUTTON_1_FIRST = 0.0;
float	S3_BUTTON_2_FIRST = 0.0;
float	S4_BUTTON_3_FIRST = 0.0;
float	S5_BUTTON_REC_FIRST = 0.0;

extern volatile uint16_t	S1_BUTTON_STOP_STATE;
extern volatile uint16_t	S2_BUTTON_1_STATE ;
extern volatile uint16_t	S3_BUTTON_2_STATE ;
extern volatile uint16_t	S4_BUTTON_3_STATE ;
extern volatile uint16_t	S5_BUTTON_REC_STATE ;

uint8_t myTxData[] = {127,128,'1', '8', '9', '7', '4', '0', '1', '5'};

uint8_t Stop_data[] = {127,128,'S', 't', 'o', 'p', '_', '_', '_', '_'};

uint8_t Record_data[] = {127,128,'R', 'e', 'c', 'o', 'r', 'd', '_'};

uint8_t Playback_data[] = {127,128,'P', 'l', 'a', 'y', '_', '_', '_'};


uint8_t wavetype = 1;



/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM1_Init(void);
static void MX_DAC_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
static void MX_ADC1_Init(void);
static void MX_SPI1_Init(void);
/* USER CODE BEGIN PFP */


void DAC_play(void);




/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

uint16_t record_buffer[1024];

uint16_t DACbuffer[1024];

uint8_t rec_state1 = 0;
uint8_t rec_state2 = 0;
uint8_t rec_state3 = 0;

uint8_t play_state1 = 0;
uint8_t play_state2 = 0;
uint8_t play_state3 = 0;

uint16_t rec_buf1[20480];
//uint16_t rec_buf2[20480];
//uint16_t rec_buf3[20480];


uint16_t record_buffer[1024];

int32_t tempsample;

int16_t outputbuf[1024];

int32_t average = 128;

int32_t numavg;

int32_t accumulator;

FATFS fs;
FRESULT fres;
FIL sdfile;

uint8_t savestart = 0; //flag variable for halfcallback
uint8_t savemid = 0; //flag variable for fullcallback








int rec_buf_pos = 0;


void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc){

	huart2.gState = HAL_UART_STATE_READY;
	if (rec_state1 == 1){


		  huart2.gState = HAL_UART_STATE_READY;


		  for(int i = 512; i < 1024; i++){


			accumulator += record_buffer[i];


				tempsample = (int32_t)record_buffer[i]- average;

				if(tempsample > 127)
					tempsample = 127;
					if(tempsample < -128)
						tempsample = -128;


					outputbuf[i] = (int8_t)tempsample;

			}

			numavg += 512;

			if(numavg >= 20480){

			average = accumulator / 20480;

			accumulator = 0;

			numavg = 0;


			}

			savemid = 1;

		}


				if(S1_BUTTON_STOP_STATE == 1){

					stop();
				}
}



void HAL_ADC_ConvHalfCpltCallback(ADC_HandleTypeDef* hadc){

	huart2.gState = HAL_UART_STATE_READY;
	if (rec_state1 == 1){


		  huart2.gState = HAL_UART_STATE_READY;


		  for(int i = 0; i < 512; i++){


			accumulator += record_buffer[i];


				tempsample = (int32_t)record_buffer[i]- average;

				if(tempsample > 127)
					tempsample = 127;
					if(tempsample < -128)
						tempsample = -128;


					outputbuf[i] = (int8_t)tempsample;

			}

			numavg += 512;

			if(numavg >= 20480){

			average = accumulator / 20480;

			accumulator = 0;

			numavg = 0;


			}


			savestart = 1;
		}

						if(S1_BUTTON_STOP_STATE == 1){

								stop();
							}
}


void HAL_DAC_ConvCpltCallbackCh1 (DAC_HandleTypeDef *hdac){


	HAL_DAC_Start_DMA(hdac,DAC_CHANNEL_1 , rec_buf1, 1024, DAC_ALIGN_12B_R);


	if (play_state1 == 1){

		for (int rec_buf_pos = 512 ; rec_buf_pos < 1024 ; rec_buf_pos++){

			HAL_UART_Transmit_DMA(&huart2, rec_buf1 + 512, 1024);

					if(S1_BUTTON_STOP_STATE == 1){

						stop();
					}

			}
		}
	}









//	wave_fillbuffer(DACbuffer + 512, wavetype, 512);



void HAL_DAC_ConvHalfCpltCallbackCh1(DAC_HandleTypeDef * hdac){



	if (play_state1 == 1){


				for (int rec_buf_pos = 0 ; rec_buf_pos < 512 ; rec_buf_pos++){

					HAL_UART_Transmit_DMA(&huart2, rec_buf1, 1024);


								if(S1_BUTTON_STOP_STATE == 1){

									stop();
								}


							}

			}
		}



//	wave_fillbuffer(DACbuffer, wavetype, 512);





/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */




  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_TIM1_Init();
  MX_DAC_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_ADC1_Init();
  MX_SPI1_Init();
  MX_FATFS_Init();
  /* USER CODE BEGIN 2 */



  HAL_ADC_Start_DMA(&hadc1,(uint32_t *)record_buffer, 1024);

  __HAL_TIM_ENABLE(&htim2);

//
//
//  uint8_t res = SD_Init();
//  if(res == 1){
//
//	  HAL_UART_Transmit(&huart2, (uint8_t*)"OK", 4, 1000);
//  }
//
//  uint8_t rxbuffer[512];
//  uint8_t txbuff[512] = "hello world!";
//
//  SD_Write(txbuff, 0, 1);
//  SD_Read(rxbuffer, 0, 1);
//  HAL_UART_Transmit(&huart2, rxbuffer, 512, 1000);
//


 fres = f_mount(&fs, "", 1); //mount SD_Card File system

 fres = f_open(&sdfile, "record1.bin", FA_CREATE_ALWAYS|FA_WRITE);





  int n;
  n = 0;
  uint8_t myTxData[] = {127,128,'1', '8', '9', '7', '4', '0', '1', '5'};

  //Initialise timer
  HAL_TIM_Base_Start(&htim2);
  wave_init();





  HAL_UART_Transmit(&huart2, "\r\n", 2, 1000);  // printing cariage return and new line character
  HAL_UART_Transmit(&huart2, myTxData, 10, 1000);
  HAL_UART_Transmit(&huart2, "\r\n", 2, 1000);  // printing cariage return and new line character



  /* USER CODE END 2 */
 
 

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  UINT num;
  int cnt = 430;
  uint8_t record;

  while (1)

  {

	 if(record){

	if(savestart)  {

		fres = f_write(&sdfile, outputbuf, 512, &num);
		savestart = 0;
		cnt--;

	}
	if(savemid){
		fres = f_write(&sdfile, outputbuf+512, 512, &num);
		savemid = 0;
		cnt--;
	}


	if(cnt == 0){
		record = 0;
		f_close(&sdfile);
	}




	 }







	  //-------------------------Button Stop---------------------------------------------------

	  if(S1_BUTTON_STOP_STATE == 1 && S1_BUTTON_STOP_FIRST == 0)
	  {

		  S1_BUTTON_STOP_FIRST = HAL_GetTick(); //the first time the button is seen by the tick counter ; initializes the tick ; this happens when button is pressed
		  // case '':
	  }
	  if(S1_BUTTON_STOP_STATE == 1 && S1_BUTTON_STOP_FIRST!=0)
	  {
		  if (HAL_GetTick() > (S1_BUTTON_STOP_FIRST + BUTTON_DEBOUNCE_DELAY)&&(!HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_4)))
		  {
			  stop();
		  }
		  if (HAL_GetTick() > (S1_BUTTON_STOP_FIRST + BUTTON_DEBOUNCE_DELAY)&&(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_4)))
		  		  {
		  			  stop();
		  		  }

	  }


	  //-------------------------Button 1---------------------------------------------------



	  if(S2_BUTTON_1_STATE == 1 && S2_BUTTON_1_FIRST == 0 )
	  {

		  S2_BUTTON_1_FIRST = HAL_GetTick(); //the first time the button is seen by the tick counter ; initializes the tick ; this happens when button is pressed

		  // case '':
	  }
	  if(S2_BUTTON_1_STATE == 1 && S2_BUTTON_1_FIRST!=0 && S5_BUTTON_REC_STATE == 0)
	  {

		  if (HAL_GetTick() > (S2_BUTTON_1_FIRST + BUTTON_DEBOUNCE_DELAY))
		  {
//			  	  wavetype = 1;
//			  	wave_fillbuffer(DACbuffer, 1, 1024);
			  	HAL_DAC_Start_DMA(&hdac,DAC_CHANNEL_1 , DACbuffer, 1024, DAC_ALIGN_12B_R);
			  	record_play(1);
//HAL_UART_Transmit_DMA(&huart2, DACbuffer, 1024);

		  }
	  }




//-----------------------------------button 2------------------------------------------------------------



	  if(S3_BUTTON_2_STATE == 1 && S3_BUTTON_2_FIRST == 0 )
	  {

		  S3_BUTTON_2_FIRST = HAL_GetTick(); //the first time the button is seen by the tick counter ; initializes the tick ; this happens when button is pressed

		  // case '':
	  }
	  if(S3_BUTTON_2_STATE == 1 && S3_BUTTON_2_FIRST!=0)
	  {


		  if (HAL_GetTick() > (S3_BUTTON_2_FIRST + BUTTON_DEBOUNCE_DELAY)&&(!HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_13)))
		  {
			  wavetype = 2;
			 wave_fillbuffer(DACbuffer, 2, 1024);
			 HAL_DAC_Start_DMA(&hdac,DAC_CHANNEL_1 , DACbuffer, 1024, DAC_ALIGN_12B_R);
			record_play(2);
		  }
	  }



//---------------------------------------------------Button 3-------------------------------------------------------------------

if(S4_BUTTON_3_STATE == 1 && S4_BUTTON_3_FIRST == 0 )
{

	  S4_BUTTON_3_FIRST = HAL_GetTick(); //the first time the button is seen by the tick counter ; initializes the tick ; this happens when button is pressed

	  // case '':
}
if(S4_BUTTON_3_STATE == 1 && S4_BUTTON_3_FIRST!=0)
{
	  if (HAL_GetTick() > (S4_BUTTON_3_FIRST + BUTTON_DEBOUNCE_DELAY)&&(!HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_13)))
	  {

		  wavetype = 3;
		  wave_fillbuffer(DACbuffer, 3, 1024);
		  HAL_DAC_Start_DMA(&hdac,DAC_CHANNEL_1 , DACbuffer, 1024, DAC_ALIGN_12B_R);
		  record_play(3);

	  }

}

//------------------------------------record-------------------------------------------------------------------------------------

if(S5_BUTTON_REC_STATE == 1 && S5_BUTTON_REC_FIRST == 0 )
{

	  S5_BUTTON_REC_FIRST = HAL_GetTick(); //the first time the button is seen by the tick counter ; initializes the tick ; this happens when button is pressed
	  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, 1);
	  // case '':
}

//------------------------------------ record if but 1-----------------------------------------------------------------
if((S5_BUTTON_REC_STATE == 1 && S5_BUTTON_REC_FIRST!=0) && (HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_13)))
{

	  if (HAL_GetTick() > (S5_BUTTON_REC_FIRST + BUTTON_DEBOUNCE_DELAY)&&(!HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_13)))
	  {

		record_start(1);


	  }

}
//------------------------------------ record if but 2-----------------------------------------------------------------
if((S5_BUTTON_REC_STATE == 1 && S5_BUTTON_REC_FIRST!=0) && (HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_14)))
{

	  if (HAL_GetTick() > (S5_BUTTON_REC_FIRST + BUTTON_DEBOUNCE_DELAY)&&(!HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_14)))
	  {


		  record_start(2);


	  }

}

//------------------------------------ record if but 3-----------------------------------------------------------------
if((S5_BUTTON_REC_STATE == 1 && S5_BUTTON_REC_FIRST!=0) && (HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_15)))
{


	  if (HAL_GetTick() > (S5_BUTTON_REC_FIRST + BUTTON_DEBOUNCE_DELAY)&&(!HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_15)))
	  {
		  record_start(3);
	  }

 }




//	  test = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_12);
//	test01 = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_13);

//	if (test == 1)
//	{
//			HAL_GPIO_TogglePin(GPIOA,GPIO_PIN_12);
//		}

 // if (test == 1) {
//		  	  uint8_t myTxData[] = {127,128,'1', '8', '9', '7', '4', '0', '1', '5'};
//		  	  HAL_UART_Transmit(&huart2, myTxData, 10, 1000);

}

  }

//}


    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
void serial_print(char* text)
{
	int i;

	i=0;
	while(text[i] != 0)
	{
		i++;
	}
	HAL_UART_Transmit(&huart2, text, i, 1000);
}

void serial_println(char* text)
{
	int i;

	i=0;
	while(text[i] != 0)
	{
		i++;
	}
	HAL_UART_Transmit(&huart2, text, i, 1000);
	HAL_UART_Transmit(&huart2, "\r\n", 2, 1000);
}

/**
  * @Function To Play Recording
  * @param pin of the button
  * @retval None
  */
void record_play(int pin)
{

	PLAY_SLOT1 = 1;
	char pin_nr[1];

	itoa(pin, pin_nr, 10);  //convert integer to string, radix=10

	HAL_UART_Transmit(&huart2, Playback_data, 9, 1000);
	HAL_UART_Transmit(&huart2, pin_nr, 1, 1000);
	HAL_UART_Transmit(&huart2, "\r\n", 2, 1000);

//		LED3_FIRST_FLASH = HAL_GetTick();



	PLAY_START_TICK = HAL_GetTick();


			if(pin == 1){

			play_state1 = 1;

			}



	while(PLAY_SLOT1 == 1)
	{


		if ((HAL_GetTick() >= PLAY_START_TICK + PLAY_DELAY_TIME) || (S1_BUTTON_STOP_STATE == 1)){


										if(pin == 1){

											play_state1 = 0;

									}

						}




		// HAL_UART_Transmit(&huart2, "flashing led 1\r\n", 17, 1000);
		if((HAL_GetTick()>= LED1_FIRST_FLASH + LED_DELAY_TIME)&&(!HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_13)))
		{
			if (pin == 1)
			{
				HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_1);
				LED1_FIRST_FLASH = HAL_GetTick();
			}
			else if (pin == 2)
			{
				  HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_8);
				  LED1_FIRST_FLASH = HAL_GetTick();
			}
			else if (pin == 3)
			{
				HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_11);
				LED1_FIRST_FLASH = HAL_GetTick();
			}
		}
		// stop function
		if(S1_BUTTON_STOP_STATE == 1 && S1_BUTTON_STOP_FIRST == 0)
		{
			S1_BUTTON_STOP_FIRST = HAL_GetTick(); //the first time the button is seen by the tick counter ; initializes the tick ; this happens when button is pressed
		}
		if(S1_BUTTON_STOP_STATE == 1 && S1_BUTTON_STOP_FIRST!=0)
		{
			if (HAL_GetTick() > (S1_BUTTON_STOP_FIRST + BUTTON_DEBOUNCE_DELAY)&&(!HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_4)))
			{
				stop();
	  		}
	  	}
	}
	S2_BUTTON_1_STATE = 0; //this is giving trouble because if the while loop is executing this value never becomes zero and the stop can't overwrite this
}

/**
  * @Function To Start Recording
  * @param pin of the button
  * @retval None
  */
void record_start(int pin)
{
	char pin_nr[1];
	itoa(pin, pin_nr, 10);  //convert integer to string, radix=10
	HAL_UART_Transmit(&huart2, Record_data, 9, 1000);
	HAL_UART_Transmit(&huart2, pin_nr, 1, 1000);
	HAL_UART_Transmit(&huart2, "\r\n", 2, 1000);

	RECORDING = 1;
	LED3_FIRST_FLASH = HAL_GetTick();



	REC_START_TICK = HAL_GetTick();


		if(pin == 1){

		rec_state1 = 1;



		}




	while(RECORDING == 1)
	{

		if ((HAL_GetTick() >= REC_START_TICK + REC_DELAY_TIME) || (S1_BUTTON_STOP_STATE == 1)){


								if(pin == 1){

									rec_state1 = 0;

							}

				}


		// --------------- flash rec led
		if((HAL_GetTick()>= LED3_FIRST_FLASH + LED_DELAY_TIME))
		{
			//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, 0);

			if (pin == 1)
			{
				HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_1);
				//HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_12);
			}
			else if (pin == 2)
			{
				  HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_8);
				//  HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_12);
			}
			else if (pin == 3)
			{
				HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_11);
			//	HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_12);
			}

			LED3_FIRST_FLASH = HAL_GetTick();
		}
		// stop funxction
		if(S1_BUTTON_STOP_STATE == 1 && S1_BUTTON_STOP_FIRST == 0)
		{
			S1_BUTTON_STOP_FIRST = HAL_GetTick(); //the first time the button is seen by the tick counter ; initializes the tick ; this happens when button is pressed

		}
		if(S1_BUTTON_STOP_STATE == 1 && S1_BUTTON_STOP_FIRST != 0)
		{
			if (HAL_GetTick() > (S1_BUTTON_STOP_FIRST + BUTTON_DEBOUNCE_DELAY)&&(!HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_4)))
		  	{
				stop();
		  	}
		}
	}
	S4_BUTTON_3_STATE = 0; //this is giving trouble because if the while loop is executing this value never becomes zero and the stop can't overwrite this
}

/**
  * @Stop Function
  * @param None
  * @retval None
  */
void stop()
{
	RECORDING = 0;
	HAL_UART_Transmit(&huart2, Stop_data, 10, 1000);
	HAL_UART_Transmit(&huart2, "\r\n", 2, 1000);
	//only execute after the button is released and debounce time
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, 0);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, 0);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, 0);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, 0);

	//	HAL_UART_Transmit(&huart2, Stop_data, 10, 1000);
	S1_BUTTON_STOP_STATE = 0;

	LED1_FIRST_FLASH = 0.0;
	LED2_FIRST_FLASH = 0.0;
	LED3_FIRST_FLASH = 0.0;
	LED4_FIRST_FLASH = 0.0;

	PLAY_SLOT1 = 0;
	PLAY_SLOT2 = 0;
	PLAY_SLOT3 = 0;

	S1_BUTTON_STOP_FIRST = 0.0;
	S2_BUTTON_1_FIRST = 0.0;
	S3_BUTTON_2_FIRST = 0.0;
	S4_BUTTON_3_FIRST = 0.0;
	S5_BUTTON_REC_FIRST = 0.0;

	S1_BUTTON_STOP_STATE = 0;
	S2_BUTTON_1_STATE = 0;
	S3_BUTTON_2_STATE = 0;
	S4_BUTTON_3_STATE = 0;
	S5_BUTTON_REC_STATE = 0;


	rec_state1 = 0;
	play_state1 = 0;

	HAL_DAC_Stop_DMA(&hdac, DAC_CHANNEL_1);
	//HAL_DAC_Stop(hdac, Channel)
	//HAL_DAC_Start_DMA(&hdac,DAC_CHANNEL_1 , DACbuffer, 1024, DAC_ALIGN_12B_R);

}


  /* USER CODE END 3 */


/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_8B;
  hadc1.Init.ScanConvMode = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;
  hadc1.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T2_TRGO;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = ENABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_15;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief DAC Initialization Function
  * @param None
  * @retval None
  */
static void MX_DAC_Init(void)
{

  /* USER CODE BEGIN DAC_Init 0 */

  /* USER CODE END DAC_Init 0 */

  DAC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN DAC_Init 1 */

  /* USER CODE END DAC_Init 1 */
  /** DAC Initialization 
  */
  hdac.Instance = DAC;
  if (HAL_DAC_Init(&hdac) != HAL_OK)
  {
    Error_Handler();
  }
  /** DAC channel OUT1 config 
  */
  sConfig.DAC_Trigger = DAC_TRIGGER_T2_TRGO;
  sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
  if (HAL_DAC_ConfigChannel(&hdac, &sConfig, DAC_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN DAC_Init 2 */

  /* USER CODE END DAC_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 32000;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 50;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 1905;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 1750;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_ENABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 500000;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Stream5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream5_IRQn);
  /* DMA1_Stream6_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream6_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream6_IRQn);
  /* DMA2_Stream0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LD2_Pin|D4_LED_3_Pin|D5_LED_REC_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, CS_Clock_Select_Pin|LED1_D2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(D3_LED_2_GPIO_Port, D3_LED_2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : LD2_Pin D4_LED_3_Pin D5_LED_REC_Pin */
  GPIO_InitStruct.Pin = LD2_Pin|D4_LED_3_Pin|D5_LED_REC_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : S1_BUTTON_STOP_Pin */
  GPIO_InitStruct.Pin = S1_BUTTON_STOP_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(S1_BUTTON_STOP_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : CS_Clock_Select_Pin LED1_D2_Pin */
  GPIO_InitStruct.Pin = CS_Clock_Select_Pin|LED1_D2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : S5_BUTTON_REC_Pin S2_BUTTON_1_Pin S3_BUTTON_2_Pin S4_BUTTON_3_Pin */
  GPIO_InitStruct.Pin = S5_BUTTON_REC_Pin|S2_BUTTON_1_Pin|S3_BUTTON_2_Pin|S4_BUTTON_3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : D3_LED_2_Pin */
  GPIO_InitStruct.Pin = D3_LED_2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(D3_LED_2_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI4_IRQn);

  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
 //   if(GPIO_Pin == S2_BUTTON_1_Pin)
//    {
//        HAL_GPIO_TogglePin (GPIOC, GPIO_PIN_4);




   }



void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(htim);

  /* NOTE : This function should not be modified, when the callback is needed,
            the HAL_TIM_PeriodElapsedCallback could be implemented in the user file
   */

}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
